import { createRouter , createWebHistory } from 'vue-router';
import Login from './components/pages/Login.vue';
import Register from './components/pages/Register.vue';
import Dashboard from './components/pages/Dashboard.vue';

const routes = [
    { path : '/login' , component : Login },
    { path : '/register' , component : Register },
    { path : '/' , component : Dashboard }
];

const router = createRouter({ history : createWebHistory(), routes });


export default router;
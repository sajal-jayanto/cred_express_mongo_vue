import User from '../models/user.js';
import bcrypt, { compareSync } from 'bcrypt';
import { create_token } from '../jwt.config.js';



export const createUser = async (req , res) => {

    const { userName , email , password , confirmPassword } = req.body;

    try {

        const is_present = await User.findOne({ email : email });
        if(is_present === null) {
           
            if(password !== confirmPassword){
                return res.status(200).json({
                
                    status : false,
                    message : `password and confirm Password don't match`
    
                });
            }

            const salt = 10;
            const haspass = bcrypt.hashSync(password , salt);
            
            const saveUser = new User({

                userName : userName,
                email : email,
                password : haspass

            });
            
            const saveData = await saveUser.save();
            const userId = saveData._id;
            const token = create_token(userId);

            return res.status(200).json({ 
                status : true,
                token : token,
                message : `User created successfully`
            });

        }
        else {
            return res.status(200).json({
                
                status : false,
                message : `This emali has been takan`

            });
        }

    } catch(err){
        return res.status(400).json({
                
            status : false,
            message : err

        });
    }

};


export const userLogin = async (req , res) => {

    const { email , password } = req.body;

    try {

        const is_present = await User.findOne({ email : email });

        if(is_present){
            
            const match = await bcrypt.compare(password, is_present.password);

            if(match) {

                const userId = is_present._id;
                const token = create_token(userId);

                return res.status(200).json({ 
                    status : true,
                    token : token,
                    message : `User login successfully`
                });

            }
            else{

                return res.status(200).json({
                
                    status : false,
                    message : `invalid password`
    
                });
            }

        }
        else {
            return res.status(200).json({
                
                status : false,
                message : `invalid emali address`

            });
        }

    } catch(err){

        return res.status(400).json({
                
            status : false,
            message : err

        });
    }

};

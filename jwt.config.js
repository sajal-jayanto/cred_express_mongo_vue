import jwt from 'jsonwebtoken';

import dotenv from 'dotenv';

dotenv.config();

export const create_token = ( userId ) => {

    const token = jwt.sign( { _id : userId } , process.env.TOKEN_SECRET , { expiresIn: '1h' });

    return token;
}

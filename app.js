import express from 'express';
import mongoose from 'mongoose';
import { connect_mongodb } from './db.config.js';
import dotenv from 'dotenv';


import userRouter from './routes/userRoute.js';




const app = express();

/// DB connection
connect_mongodb(mongoose);

dotenv.config();



app.use(express.json());


app.use('/user' , userRouter);


const port = process.env.PORT ||  5000;

app.listen(port , () => {
    console.log(`Sarver strat at port ${port}`);
});
